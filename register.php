<?php
include_once("form-action.php");
$obj = new action();

if (isset($_POST['user_reg_btn'])){
    $msg = $obj->user_register($_POST);
}

if(isset($_SESSION['id'])){
    $id = $_SESSION['id'];
    if($id){
        header('location:index.php');
    }
}

?>

<!DOCTYPE html>
<html>
<title>Mini Blog Site - Register</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/style.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<body>

    <?php include ("include/header.php"); ?>

    <div class="d-flex align-items-center justify-content-center mt-5">
        <div>
        <h3>Registration</h3>
        </div>
    </div>
    

    <div class="container mt-5" style="border:1px solid #cecece;">

    <form action="" autocomplete="off" method="POST">

    <?php
    $n=10;
    function getUserid($n) {
    $characters = '0123456789';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }
    return $randomString;
    } ?>

    <input hidden type="text" class="form-control" id="id" name="user_id" value="<?php echo getUserid($n); ?>">
    
    <h4 class="text-danger" style="color: black;"> <center><?php 
                    if(isset($msg)){
                        echo $msg;
                    }
                ?></center></h4>
    <br>
    <h3>See the Registration Rules</h3>
    <hr>
    
    <div class="mb-3 mt-3">
    <label for="username" class="form-label">Username:</label>
        <input type="text" class="form-control" id="username" placeholder="Enter username" name="reg_user">
    </div>

    <div class="mb-3 mt-3">
    <label for="email" class="form-label">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="reg_email">
    </div>

    <div class="mb-3">
    <label for="pwd" class="form-label">Password:</label>
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="reg_pswd">
    </div>

    
    <div class="mb-3">
    <label for="c_pwd" class="form-label">Confirm Password:</label>
        <input type="password" class="form-control" id="c_pwd" placeholder="Confirm password" name="c_pswd">
    </div>
    
    <button type="submit" name="user_reg_btn" class="btn btn-primary">Register</button> 
    </form>

    <br>
    <p>Return to the <a href="login.php">LOGIN PAGE</a></p>
    </div>

</body>
</html>


