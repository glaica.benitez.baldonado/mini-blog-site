<?php
session_start();
include_once("form-action.php");
$obj = new action();

if(isset($_GET['status'])){
    if($_GET['status']=='edit'){
        $id = $_GET['id'];
        $st = $obj->show_postId($id);
    }
}


if(isset($_POST['update_content'])){
    $msg = $obj->update_postId($_POST);

}
?>

<!DOCTYPE html>
<html>
<title>Mini Blog Site - Edit Post</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<body>

    <?php include ("include/header.php"); ?>

    
    
    <div class="container mt-5" style="">

        <?php 
            if(isset($msg)){ ?>
                <div class="alert alert-primary">
                <?php echo $msg; ?>
                </div>
                    <?php }
                ?>

    

    <form action="" autocomplete="off" method="POST"> 
    
    <?php 
    while ($edit_p = mysqli_fetch_assoc($st)){ 
    ?>

<div class="d-flex align-items-left justify-content-left mt-5">
        <div>
        <h3>Edit Post - <?php echo $edit_p['post_title'] ?></h3>
        </div>
    </div>

<input hidden type="text" class="form-control" id="id" name="u_post_id" value="<?php echo $edit_p['post_id'] ?>">

    <div class="mb-3 mt-3">
    <label for="post_title" class="form-label">Enter Title:</label>
        <input type="text" class="form-control" id="post_title" placeholder="Enter Title" name="u_post_title" value="<?php echo $edit_p['post_title'] ?>">
    </div>

    <div class="mb-3">
    <label for="content" class="form-label">Enter Content:</label>
    <textarea class="form-control" id="textarea" rows="5" name="u_post_cnt">
    <?php echo $edit_p['post_content'] ?>
    </textarea>
    </div>

    <?php } ?>
    <button type="submit" name="update_content" class="btn btn-success">SAVE</button> 
    </form>

    </div>
<body>
    </html>