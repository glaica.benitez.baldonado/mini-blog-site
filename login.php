<?php
include_once("form-action.php");
$obj = new action();

if (isset($_POST['user_login_btn'])){
    $logmsg = $obj->user_login($_POST);
}

if(isset($_SESSION['id'])){
    $id = $_SESSION['id'];
    if($id){
        header('location:index.php');
    }
}

?>

<!DOCTYPE html>
<html>
<title>Mini Blog Site - Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<body>

    <?php include ("include/header.php"); ?>

    <div class="container mt-5" style="border:1px solid #cecece;">
    <form action="" autocomplete="off" method="POST">
    
    <br>
    <h3>Login</h3>
    <hr>
    
    <h6 class="text-danger" style="color: black;"><?php 
                    if(isset($logmsg)){
                        echo $logmsg;
                    }
                ?></h6>
    <div class="mb-3 mt-3">
    <label for="email" class="form-label">Email:</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
    </div>

    <div class="mb-3">
    <label for="pwd" class="form-label">Password:</label>
        <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
    </div>
    
    <button type="submit" name="user_login_btn" class="btn btn-primary">Login</button>
    <button type="button" onclick="window.location.href='register.php'" class="btn btn-primary">Register</button>
    <br>
    </form>
    <br>
    
    </div>

    </body>

</html>


