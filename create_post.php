<?php
session_start();
include_once("form-action.php");
$obj = new action();

if (isset($_POST['post_content'])){
    $msg = $obj->create_post($_POST);
}

?>

<!DOCTYPE html>
<html>
<title>Mini Blog Site - Create Post</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<body>

    <?php include ("include/header.php"); ?>

    
    
    <div class="container mt-5" style="">

        <?php 
            if(isset($msg)){ ?>
                <div class="alert alert-primary">
                <?php echo $msg; ?>
                </div>
                    <?php }
                ?>

    <div class="d-flex align-items-left justify-content-left mt-5">
        <div>
        <h3>Create New Post</h3>
        </div>
    </div>

    <form action="" autocomplete="off" method="POST">

    
    <?php
    $n=5;
    function getPostid($n) {
    $characters = 'abc0123456789';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }
    return $randomString;
    } ?>

<input hidden type="text" class="form-control" id="id" name="post_id" value="<?php echo getPostid($n); ?>">

    <div class="mb-3 mt-3">
    <label for="post_title" class="form-label">Enter Title:</label>
        <input type="text" class="form-control" id="post_title" placeholder="Enter Title" name="post_title">
    </div>

    <div class="mb-3">
    <label for="content" class="form-label">Enter Content:</label>
    <textarea class="form-control" id="textarea" rows="3" name="post_cnt"></textarea>
    </div>
    
    <button type="submit" name="post_content" class="btn btn-primary">POST</button> 
    </form>

    </div>
<body>
    </html>