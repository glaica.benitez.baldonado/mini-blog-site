-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2023 at 10:35 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miniblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(5) NOT NULL,
  `post_id` varchar(10) NOT NULL,
  `post_title` varchar(225) NOT NULL,
  `post_content` text NOT NULL,
  `date_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `post_id`, `post_title`, `post_content`, `date_posted`) VALUES
(2, '120c7', 'Lorem Ipsum Sample 1', ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu iaculis massa. Etiam in erat elit. Mauris posuere volutpat magna, ut dignissim leo sollicitudin gravida. Mauris maximus vel magna vel vestibulum. Morbi vestibulum lobortis massa, eu ultricies mauris cursus sit amet. Phasellus in odio sed tellus fringilla accumsan at consequat libero. Proin malesuada id sem eget scelerisque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Quisque dignissim accumsan lobortis. Maecenas quis ornare leo, at pharetra est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean ex quam, vulputate non massa nec, ullamcorper dictum leo. Aliquam nisi nunc, commodo scelerisque suscipit a, euismod ac augue.                    ', '2023-04-24 14:53:56'),
(3, 'b2071', 'Sample', 'Integer neque est, semper nec aliquet ut, tempus id justo. Vivamus a velit ut tellus laoreet viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras laoreet viverra neque, id lobortis lectus sollicitudin vel. Duis et risus augue. Proin id ultrices orci, vitae auctor sem. Ut finibus molestie massa, et volutpat metus venenatis a. Donec ornare metus ut urna viverra porttitor. Pellentesque arcu odio, dignissim in ipsum mattis, molestie finibus ligula. Nunc dictum sagittis sollicitudin. Mauris ornare condimentum massa, in volutpat diam posuere a. Pellentesque lobortis auctor ornare. Ut malesuada urna et sem egestas luctus. Nam est eros, imperdiet eu finibus ut, venenatis dictum leo.', '2023-04-24 14:59:03'),
(4, '70815', 'Sample Post', 'Sample post', '2023-04-24 16:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(5) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `user_uname` varchar(225) NOT NULL,
  `user_email` varchar(225) NOT NULL,
  `user_password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_uname`, `user_email`, `user_password`) VALUES
(1, '8522404651', 'user', 'usersample@gmail.com', 'usersample');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
