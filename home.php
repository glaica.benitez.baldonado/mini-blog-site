<?php
session_start();
include_once("form-action.php");
$obj = new action();

$post = $obj-> display_post();

if(isset($_GET['status'])){
    $get_id = $_GET['id'];
    if($_GET['status']=="delete"){
        $del_msg = $obj->delete_post($get_id);
    }
    }

?>

<!DOCTYPE html>
<html>
<title>Mini Blog Site - Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" 
rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<body>

    <?php include ("include/header.php"); ?>
    
    <?php 
    while ($p = mysqli_fetch_assoc($post)){ 
    ?>
    <div class="container mt-5" style="">



        <div class="card">
            <div class="card-body">

            <input hidden type="text" class="form-control" id="id" name="post_id" value="<?php echo $p['post_id'] ?>">
            <h1 class="display-6"><?php echo $p['post_title'] ?></h1>
                
                <p><?php echo $p['post_content'] ?></p>
                Date: <?php  echo date('h:i:s a m/d/Y', strtotime($p['date_posted'])); ?>
            </div>
            <div class="card-footer">
            <a href="?status=delete&&id=<?php echo $p['post_id'] ?>" type="button" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this service? NOTE: This action cannot be undone.')">Delete</a>
            <a href="edit_post.php?status=edit&&id=<?php echo $p['post_id']?>" type="button" class="btn btn-success">Edit</a>
            </div>
        </div>
    </div>
    <?php } ?>


    <div class="container mt-5" style="">
        <div class="card">
            <div class="card-body">
            <a href="create_post.php" type="button" class="btn btn-primary">CREATE NEW POST</a>
            </div>
        </div>
    </div>

    <br>
<body>

<script>
$("time").each(function(){
    var date = $(this).text();
    var format_date = moment(date).format('MMMM Do YYYY, h:mm:ss');    

    $(this).text(format_date);
    $(this).attr("datetime", format_date);
});
        </script>
        
    </html>