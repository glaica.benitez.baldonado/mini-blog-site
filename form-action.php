<?php

class  action
{
    private $connection;
    
    function __construct()
    {
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = "";
        $dbname = "miniblog";

        $this->connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

        if (!$this->connection) {
            die("Database connection error!");
        }
    }

    function user_login($data)
    {
        $user_email = $data["email"];
        $user_password = $data['pswd'];

        $query = "SELECT * FROM `users` WHERE user_email = '$user_email' AND user_password = '$user_password'";

        if (mysqli_query($this->connection, $query)) {
            $result = mysqli_query($this->connection, $query);
            $user_info = mysqli_fetch_assoc($result);
            if ($user_info) {
                header("location:home.php");
                session_start();

                $_SESSION['user_id'] = $user_info['user_id'];
                $_SESSION['user_email'] = $user_info['user_email'];
                $_SESSION['user_uname'] = $user_info['user_uname'];
            } else {
                $log_msg = "You have entered a wrong email or password. Please try again.";
                return $log_msg;
            }
        }
    }

    function user_logout()
    {
        unset($_SESSION['admin_id']);
        unset($_SESSION['admin_email']);
        unset($_SESSION['role']);
        header("location:index.php");
        session_destroy();
    }


    function user_register($data)
    {
        $user_id = $data['user_id'];
        $username = $data['reg_user'];
        $user_email = $data['reg_email'];
        $user_password = $data['reg_pswd'];
        $user_c_pass = $data['c_pswd'];

        $user_check = "SELECT * FROM `users` WHERE user_uname='$username' or user_email='$user_email'";

        $mysqli_result = mysqli_query($this->connection, $user_check);

        $row = mysqli_num_rows($mysqli_result);

        if ($row == 1) {
            $msg = "Username or email already exist.";
            return $msg;
            
        }
        elseif($user_password != $user_c_pass){
            $msg = "Passwords doesn't match.";
        }
        else {
            $query = "INSERT INTO `users`
            ( `user_id`, 
            `user_uname`, 
            `user_email`, 
            `user_password`)
            VALUES (
            '$user_id',
            '$username',
            '$user_email',
            '$user_password')";

            if (mysqli_query($this->connection, $query)) {
                $msg = "Your registration done.";
                return $msg;
            }
        }
    }

    //END USER

    //POST
    function display_post(){
        $query = "SELECT * FROM `post`";
        if(mysqli_query($this->connection, $query)){
            $result = mysqli_query($this->connection, $query);
            return $result;
        }
    }

    function show_postId($id){
        $query = "SELECT * FROM `post` WHERE `post_id`= '$id'";
        if(mysqli_query($this->connection, $query)){
            $result = mysqli_query($this->connection, $query);
            return $result;
        }
    }

    function create_post($data)
    {
        $post_id = $data['post_id'];
        $post_title = $data['post_title'];
        $post_cnt = $data['post_cnt'];

        $query = "INSERT INTO `post`
        ( `post_id`, `post_title`,`post_content`,`date_posted`) 
        VALUES ('$post_id','$post_title','$post_cnt', NOW())";

        if (mysqli_query($this->connection, $query)) {
            $msg = "{$post_title} successfully posted.";
            return $msg;
        } else {
            $msg = "{$post_title}: failed tp post.";
            return $msg;
        }
    }

    function update_postId($data)
    {
        $u_post_title = $data['u_post_title'];
        $u_post_cnt = $data['u_post_cnt'];
        $post_id= $data['u_post_id'];


        $query = "UPDATE `post` SET 
        `post_title`='$u_post_title',
        `post_content`='$u_post_cnt'
        WHERE post_id= '$post_id'";
        if (mysqli_query($this->connection, $query)) {
            $msg = "{$u_post_title} successfully updated.";
            echo "<script type='text/javascript'>
            alert('$msg');
            document.location.href='home.php';
            </script>";
            
        }
    }

    function delete_post($id)
    {

        $query = "DELETE FROM `post` WHERE  post_id = '$id'";
        if (mysqli_query($this->connection, $query)) {
            if (isset($_SERVER["HTTP_REFERER"])) {
                header("Location: " . $_SERVER["HTTP_REFERER"]);
            }
        
    }
    }

}